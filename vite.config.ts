import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import {resolve} from "path"
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'
import {readFileSync} from "fs";
import path from "path";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    AutoImport({
      resolvers: [ElementPlusResolver()],
    }),
    Components({
      resolvers: [ElementPlusResolver()],
    }),
  ],
  server:{
    host: "0.0.0.0",
    https: {
      cert: readFileSync(path.join(__dirname, "keys/cert.crt")),
      key: readFileSync(path.join(__dirname, "keys/cert.key"))
    },
    proxy: {
      '/media-server' : {
        // target: "http://10.10.40.44:8083",
        target: "http://10.10.100.30:80",
        changeOrigin:true,
        ws: true,
      },
    }
  },
  resolve: {
    alias: [
      {
        find: "@",
        replacement: resolve(__dirname, "src")
      }
    ]
  }
})
