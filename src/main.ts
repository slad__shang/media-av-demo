import { createApp } from 'vue'
import App from './App.vue'
import './assets/styles/index.scss'
import 'element-plus/theme-chalk/dark/css-vars.css'

const app = createApp(App)


app.mount('#app')